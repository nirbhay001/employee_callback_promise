let {dataId,groupData,PowerpuffData,entryId2,sortData,swapCompany,birthdate}=require("./employee.cjs")
let data = require("./data.json");


dataId(data)
.then((data)=>{
    return groupData(data);
})
.then((data)=>{
    return PowerpuffData(data);
})
.then((data)=>{
    return entryId2(data);
})
.then((data)=>{
   return sortData(data);
})
.then((data)=>{
    return swapCompany(data);
})
.then((data)=>{
    return birthdate(data);
})
.then((data)=>{
    console.log(data);
})
.catch((error)=>{
    console.log(error);
})


