let path = require('path');
let fs = require('fs');

function dataId(data) {
    return new Promise((resolve,reject)=>{
        let pathFile = path.join(__dirname, "ids.json");
        let result = data.employees.filter((item) => {
            if (item.id === 2 || item.id === 13 || item.id === 23) {
                return item;
            }
        })
        fs.writeFile(pathFile, JSON.stringify(result), (error) => {
            if (error) {
                reject(error);
            }
            else {
                resolve(data);
            }
        })

    })

};

function groupData(data) {
    return new Promise((resolve,reject)=>{
        let pathFile = path.join(__dirname, "groupCompany.json");
        let result = data.employees.reduce((prev, curr) => {
            if (prev[curr.company] === undefined) {
                prev[curr.company] = [];
                prev[curr.company].push(curr);
            }
            else {
                prev[curr.company].push(curr);
            }
            return prev;
        }, {})
        fs.writeFile(pathFile, JSON.stringify(result), (error) => {
            if (error) {
                reject(error)
            }
            else {
                resolve(data);
            }
        })
    })

}

function PowerpuffData(data) {
    return new Promise((resolve,reject)=>{
        let pathFile = path.join(__dirname, "brigadeCompany.json");
        let result = data.employees.filter((item) => {
            return item.company === "Powerpuff Brigade";
        })
        fs.writeFile(pathFile, JSON.stringify(result), (error) => {
            if (error) {
                reject(error);
            }
            else {
                resolve(data);
            }
        })

    })

}

function entryId2(data) {
    return new Promise((resolve,reject)=>{
        let pathFile = path.join(__dirname, "removeId2.json");
        let result = data.employees.filter((item) => {
            return item.id !== 2;
        })
        fs.writeFile(pathFile, JSON.stringify(result), (error) => {
            if (error) {
                reject(error)
            }
            else {
                resolve(data);
            }
        })
    })

}

function sortData(data) {
    return new Promise((resolve, reject) => {
        let pathFile = path.join(__dirname, "sortData.json");
        let result = data.employees.sort((item1, item2) => {
            if (item1.company < item2.company) {
                return -1;
            }
            else if (item1.company > item2.company) {
                return 1;
            }
            else {
                if (item1.id < item2.id) {
                    return -1;
                }
                else if (item1.id > item2.id) {
                    return 1;
                }
                else {
                    return 0;
                }
            }
        })
        fs.writeFile(pathFile, JSON.stringify(result), (error) => {
            if (error) {
                reject(error)
            }
            else {
                resolve(data);
            }
        })
        
    })

}

function swapCompany(data) {
    return new Promise((resolve,reject)=>{
        let pathFile = path.join(__dirname, "swapData.json");
        let result1 = data.employees.filter((item) => {
            return item.id === 92;
        })
        let result2 = data.employees.filter((item) => {
            return item.id === 93;
        })
        let newResult = data.employees.map((item) => {
            if (item.id === 92) {
                return result2;
            }
            else if (item.id === 93) {
                return result1;
            }
            return item;
        })
        fs.writeFile(pathFile, JSON.stringify(newResult), (error) => {
            if (error) {
                reject(error);
                return;
            }
            else {
                resolve(data);
            }
        })
    })
    

}

function birthdate(data) {
    return new Promise((resolve, reject) => {
        let pathFile = path.join(__dirname, "birthdayDate.json");
        let result = data.employees.map((item) => {
            if ((item.id) % 2 == 0) {
                item["birthday"] = new Date().getDate();
                return item;
            }
            return item;
        })
        fs.writeFile(pathFile, JSON.stringify(result), (error) => {
            if (error) {
                reject(error)
            }
            else {
                resolve("all file created succesfully");
            }
        })

    })

}

module.exports={dataId,groupData,PowerpuffData,entryId2,sortData,swapCompany,birthdate};